#!/bin/bash
set -euxo pipefail

apt-get update
apt-get install -y gcc make wget autoconf


#install postgresql client
apt-get install -y zlib1g-dev 

PGSQL_VERSION="10.2"
PGSQL_FILENAME="postgresql-${PGSQL_VERSION}.tar.gz"

wget "https://ftp.postgresql.org/pub/source/v${PGSQL_VERSION}/${PGSQL_FILENAME}" -O "/tmp/${PGSQL_FILENAME}"
tar -xf "/tmp/${PGSQL_FILENAME}" -C "/tmp/"

cd "/tmp/postgresql-${PGSQL_VERSION}"

"./configure" \
  --prefix=/usr/local/pgsql \
  --without-readline
make -j 2
make install


#install nginx
apt-get install -y libpcre3-dev zlib1g-dev 

NGINX_VERSION="nginx-1.12.2"
NGINX_FILENAME="${NGINX_VERSION}.tar.gz"

wget "http://nginx.org/download/${NGINX_FILENAME}" -O "/tmp/${NGINX_FILENAME}"
tar -xf "/tmp/${NGINX_FILENAME}" -C "/tmp/"
cd "/tmp/${NGINX_VERSION}"

"./configure" \
  --prefix=/usr/local/nginx \
  --error-log-path=/var/log/nginx/error.log \
  --http-log-path=/var/log/nginx/access.log \
  --user=www-data \
  --group=www-data \
  --without-http_proxy_module
make -j 2 && make install


#install php7
apt-get install -y libxml2-dev curl libcurl3 libcurl4-openssl-dev libssl-dev 

PHP_VERSION="php-7.2.2"
PHP_FILENAME="${PHP_VERSION}.tar.gz"

wget "http://php.net/get/${PHP_FILENAME}/from/this/mirror" -O "/tmp/${PHP_FILENAME}"
tar -xf "/tmp/${PHP_FILENAME}" -C "/tmp/"
cd "/tmp/${PHP_VERSION}"

"./configure" \
        --prefix=/usr/local/php-fpm \
        --disable-cgi \
        --without-pear \
        --with-libdir=/lib/x86_64-linux-gnu \
        --with-pgsql=/usr/local/pgsql \
        --with-pdo-pgsql=/usr/local/pgsql \
        --with-curl \
        --with-openssl \
        --enable-mbstring \
        --enable-opcache --enable-fpm --with-fpm-user=www-data --with-fpm-group=www-data
make && make install
mkdir -p /var/webapp/public
mkdir /var/log/php
ln -s /usr/local/php-fpm/bin/php /usr/bin/php


#install supervisor
apt-get install -y supervisor

rm -r /tmp/*