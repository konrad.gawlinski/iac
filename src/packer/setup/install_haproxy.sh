#!/bin/bash
set -euxo pipefail

apt-get update
apt-get install -y gcc make wget autoconf


#install haproxy
HAPROXY_VERSION="1.8.8"
HAPROXY_FILENAME="haproxy-${HAPROXY_VERSION}.tar.gz"

mkdir -p /usr/local/haproxy
mkdir -p /var/lib/haproxy

wget "http://www.haproxy.org/download/1.8/src/${HAPROXY_FILENAME}" -O "/tmp/${HAPROXY_FILENAME}"
tar -xf "/tmp/${HAPROXY_FILENAME}" -C "/usr/local/haproxy/"
cd "/usr/local/haproxy/haproxy-${HAPROXY_VERSION}"

make TARGET=linux2628

ln -s /usr/local/haproxy/haproxy-${HAPROXY_VERSION}/haproxy /usr/bin/haproxy

mkdir -p /etc/haproxy
useradd -r --user-group haproxy

#install supervisor
apt-get install -y supervisor

rm -r /tmp/*
