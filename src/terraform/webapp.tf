provider "docker" {
  host = "unix://var/run/docker.sock"
}

resource "docker_network" "private_network" {
  name = "production_net"
}

# Create a container
resource "docker_container" "webapp" {
  image = "private/webapp:latest"
  name  = "working_webapp"
  networks = ["${docker_network.private_network.id}"]
  command = ["supervisord", "-n"]
}

# Create a container
resource "docker_container" "webapp_2" {
  image = "private/webapp:latest"
  name  = "working_webapp_2"
  networks = ["${docker_network.private_network.id}"]
  command = ["supervisord", "-n"]
}

# Create a container
resource "docker_container" "haproxy" {
  image = "private/haproxy:latest"
  name  = "webapp_haproxy_lb"
  networks = ["${docker_network.private_network.id}"]
  command = ["supervisord", "-n"]
}

# Create a container
#resource "docker_container" "webapp_3" {
#  image = "private/webapp:latest"
#  name  = "working_webapp_3"
#  networks = ["${docker_network.private_network.id}"]
#  command = ["supervisord", "-n"]
#}